package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                this.guild = guild;
                this.guild.add(this);
                //ToDo: Complete Me
        }
        public void update(){
                if(guild.getQuestType().equals("Rumble") || guild.getQuestType().equals("Escort") || guild.getQuestType().equals("Delivery")){
                        this.getQuests().add(guild.getQuest());
                }
        };

        //ToDo: Complete Me
}
