package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;

    public AdventurerServiceImpl(AdventurerRepository adventurerRepository,
                                 StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        AttackBehavior newattack;
        DefenseBehavior newdefend;
        Adventurer changee = findByAlias(alias);
        if(attackType.equals("AttackWithGun")){
            newattack = new AttackWithGun();
            changee.setAttackBehavior(newattack);
        }
        else if(attackType.equals("AttackWithSword")){
            newattack = new AttackWithSword();
            changee.setAttackBehavior(newattack);
        }
        else if(attackType.equals("AttackWithMagic")){
            newattack = new AttackWithMagic();
            changee.setAttackBehavior(newattack);
        }
        if(defenseType.equals("DefendWithShield")){
            newdefend = new DefendWithShield();
            changee.setDefenseBehavior(newdefend);
        }
        else if(defenseType.equals("DefendWithBarrier")){
            newdefend = new DefendWithBarrier();
            changee.setDefenseBehavior(newdefend);
        }
        else if(defenseType.equals("DefendWithArmor")){
            newdefend = new DefendWithArmor();
            changee.setDefenseBehavior(newdefend);
        }

        //ToDo: Complete me
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
